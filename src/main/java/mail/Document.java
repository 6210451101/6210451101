package mail;

public class Document extends Letter {

    private String important;

    public Document(String recipName, String floor, String roomNum, String sender, String important , String width, String length, String image ) {
        super(recipName,floor,roomNum,sender,width,length,image);
        this.important = important;
    }

    public String getImportant() {
        return important;
    }

    @Override
    public String toString() {
        return "Recipient Name : " + getRecipName() +
                "\nRoom :" + getRoomNum() +
                "\nSent from : " + getSender() +
                "\nImportant : " + important +
                "\nWidth :" + getWidth() + " , " +
                "Length :" + getLength() ;
    }

}
