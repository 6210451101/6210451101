package mail;

public class Supplies extends Letter {
    private String courier;
    private String trackNum;

    private String height;

    public Supplies(String recipName, String floor, String roomNum, String sender, String courier, String trackNum, String width, String length, String height, String image) {
        super(recipName, floor, roomNum, sender, width, length, image);
        this.courier = courier;
        this.trackNum = trackNum;
        this.height = height;
    }

    public String getCourier() {
        return courier;
    }
    public String getTrackNum() {
        return trackNum;
    }

    public String getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Recipient Name : " + getRecipName() +
                "\nRoom : " + getRoomNum() +
                "\nSent from : " + getSender() +
                "\nWidth : " + getWidth() + " , " +
                "Length : " + getWidth() + " , " +
                "Height : " + height +
                "\nTracking number : " + trackNum +
                "\nCourier : " + courier;
    }

}
