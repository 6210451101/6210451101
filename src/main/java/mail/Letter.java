package mail;

public class Letter{
    private String recipName;
    private String floor;
    private String roomNum;
    private String sender;
    private String width;
    private String length;
    private String image;

    public Letter(String recipName, String floor, String roomNum, String sender, String width, String length, String image) {
        this.recipName = recipName;
        this.floor = floor;
        this.roomNum = roomNum;
        this.sender = sender;
        this.width = width;
        this.length = length;
        this.image = image;
    }

    public String getRecipName() { return recipName; }

    public String getFloor() { return floor; }

    public String getRoomNum() { return roomNum; }

    public String getSender() { return sender; }

    public String getWidth() { return width; }

    public String getLength() { return length; }

    public String getImage() { return image; }

    @Override
    public String toString() {
        return "Recipient Name : " + recipName +
                "\nRoom :" + roomNum +
                "\nSent from : " + sender +
                "\nWidth :" + width + " , " +
                "Length :" + length ;
    }


}
