package person;

import java.nio.file.Path;

public class User extends Person {
    private String status;
    private String time;
    private String img;

    public User(String status,String name, String username, String password,String img) {
        super(name, username, password);
        this.img = img;
        this.status = status;
    }

    public String getName() {
        return name;
    }
    public String getPassword() { return password; }
    public String getUsername() { return username; }
    public String getImg() { return img; }

    public void setUsername(String username) { this.username = username; }
    public void setPassword(String password) { this.password = password; }
    public void setName(String name) { this.name = name; }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "username = " + username +
                ", password = " + password + ", Time = " +  time;
    }

    @Override
    public String myPos() {
        return "I am a staff.";
    }
}
