package person;

public class Visitor extends Person {
    private String phone;
    private String type;
    private String floor;
    private String roomNum;
//    private AccountVisitor check;
//    private CheckNormalRoom nor;
//    private CheckTwoBedRoom two;

    public Visitor(String name, String username, String password,String phone,String type,String floor,String roomNum) {
        super(name, username, password);
        this.phone = phone;
        this.type = type;
        this.floor = floor;
        this.roomNum = roomNum;
    }

    public String getPhone() { return phone; }

    public String getType() { return type; }

    public String getFloor() { return floor; }

    public String getRoomNum() { return roomNum; }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Username : " + getUsername() + "\nName : " + getName();
    }

    @Override
    public String myPos() {
        return "I am a visitor.";
    }
}
