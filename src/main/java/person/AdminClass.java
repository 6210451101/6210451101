package person;

public class AdminClass extends Person {
    private String phoneNumber;

    public AdminClass(String name,String username, String password) {
        super(name,username,password);
        this.phoneNumber = "08xxxxxxxx";
    }

    public String getName() {
        return name;
    }
    public String getPassword() { return password; }
    public String getUsername() { return username; }

    public void setUsername() { this.username = username; }
    public void setPassword(String password) { this.password = password; }
    public void setName() { this.name = name; }

    @Override
    public void status() {
        System.out.println("I am a condo administrator.");
    }

    @Override
    public String toString() {
        return
                "Name = "+name +", Username = "+ username + ", Password = " + password ;
    }

    @Override
    public String myPos() {
        return "I am a admin";
    }
}
