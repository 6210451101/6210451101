package condo;

import controller.personcontroller.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import person.User;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import java.time.LocalDate;

public class Admin {
    @FXML
    private Label userField,firstField,lastField,passField;
    @FXML
    private ImageView put;
    @FXML
    private Button browseBtn;
    @FXML
    TextField text,text1,idText,passText,firstname,lastname;
    private UserDataSource addAcc;
    private Account acc;
    private String img;

    private UserVisitorDataSource addVis;
    private AccountVisitor vis;

    private UserAdminDataSource addAdm;
    private AccountAdmin adm;
    public void setBrowse(String img){this.img = img; }
    @FXML
    public void initialize() {
        addAcc = new AccountFileDataSource("Data","account.csv");
        acc = addAcc.getAccountData();

        addAdm = new AccountAdminFileDataSource("Data","accountAdmin.csv");
        adm = addAdm.getAccountData();

        addVis = new AccountVisitorFileDataSource("Data","accountVisitor.csv");
        vis = addVis.getAccountData();

        text.setDisable(true);
        text.setStyle("-fx-opacity: 1");
    }
    @FXML
    public void handleBrowseBtnOnAction(ActionEvent event){
        final FileChooser chooser = new FileChooser();
//        Stage stage = (Stage) put.getScene().getWindow();

        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File file = chooser.showOpenDialog(browseBtn.getScene().getWindow());

        if (file != null){
            text.setText(file.getPath());
            try {
                // CREATE FOLDER IF NOT EXIST
                File destDir = new File("images");
                destDir.mkdirs();

                // RENAME FILE
                String[] fileSplit = file.getName().split("\\.");
                String filename = LocalDate.now()+"_"+System.currentTimeMillis()+"."+fileSplit[fileSplit.length - 1];
                Path target = FileSystems.getDefault().getPath(destDir.getAbsolutePath()+System.getProperty("file.separator")+filename);
                // COPY WITH FLAG REPLACE FILE IF FILE IS EXIST
                File f =target.toFile();
                Files.copy(file.toPath(), target, StandardCopyOption.REPLACE_EXISTING );
                // SET NEW FILE PATH TO IMAGE
                put.setFitHeight(150);
                put.setPreserveRatio(true);
                put.setImage(new Image(target.toUri().toString()));
                text1.setText(f.toURI().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }public void handleCancelBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/all_staff.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }public void handleFinishBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        if(acc.checkUser(idText.getText()) && idText.getText().length()>0 && passText.getText().length()>0
        && firstname.getText().length() > 0 && lastname.getText().length() > 0 && text.getText().length() > 0){
            User a = new User("Available",firstname.getText()+" "+lastname.getText(),idText.getText(),passText.getText(), text1.getText());

            acc.addAccount(a);
            addAcc.setAccountData(acc);
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/all_staff.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            stage.show();
        }if(!acc.checkUser(idText.getText()) || !adm.checkUser(idText.getText()) || !vis.checkUser(idText.getText())){
            userField.setText("Username has been used.");
        }else if(idText.getText().length()==0){
            userField.setText("Please enter username.");
        }else{
            userField.setText("");

        }if(passText.getText().length()==0){
            passField.setText("Please enter password.");
        }else{
            passField.setText("");
        }if(firstname.getText().length()==0){
            firstField.setText("Please enter first name.");
        }else{
            firstField.setText("");
        }if(lastname.getText().length() == 0){
            lastField.setText("Please enter last name.");
        }else{
            lastField.setText("");
        }
    }
}