package condo;

import controller.personcontroller.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class ChangePassword {
    @FXML
    private TextField username,old,newPass,confirm;
    @FXML
    private Label one,two,three,four;
    private Account acc;
    private UserDataSource addAcc;

    private AccountAdmin adm;
    private UserAdminDataSource addAdm;

    private AccountVisitor vis;
    private UserVisitorDataSource addVis;

    @FXML
    public void initialize() {
        addAcc = new AccountFileDataSource("Data","account.csv");
        acc = addAcc.getAccountData();
        addAdm = new AccountAdminFileDataSource("Data","accountAdmin.csv");
        adm = addAdm.getAccountData();
        addVis = new AccountVisitorFileDataSource("Data","accountVisitor.csv");
        vis = addVis.getAccountData();
    }
    public void handleConfirmBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        if((acc.checkForChange(username.getText(),old.getText())==1)&& newPass.getText().equals(confirm.getText())
            && username.getText().length() > 0
                && old.getText().length() > 0
                && newPass.getText().length() > 0
                && confirm.getText().length() > 0
        ){


            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to change password");
            alert.setContentText("Are you sure you want to change the password?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                acc.setNewPassword(username.getText(),newPass.getText());
                addAcc.setAccountData(acc);
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                stage.show();
            } else {
                alert.close();
            }

        }else if((adm.checkForChange(username.getText(),old.getText())==1) && newPass.getText().equals(confirm.getText())
                && username.getText().length() > 0
                && old.getText().length() > 0
                && newPass.getText().length() > 0
                && confirm.getText().length() > 0
        ){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to change password");
            alert.setContentText("Are you sure you want to change the password?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                adm.setNewPassword(username.getText(),newPass.getText());
                addAdm.setAccountData(adm);
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                stage.show();
            } else {
                alert.close();
            };
        }else if((vis.checkForChange(username.getText(),old.getText())==1) && newPass.getText().equals(confirm.getText())
                && username.getText().length() > 0
                && old.getText().length() > 0
                && newPass.getText().length() > 0
                && confirm.getText().length() > 0
        ){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to change password");
            alert.setContentText("Are you sure you want to change the password?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                vis.setNewPassword(username.getText(),newPass.getText());
                addVis.setAccountData(vis);
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                stage.show();
            } else {
                alert.close();
            }

        }if((username.getText().length() > 0 && old.getText().length()>0) && (!acc.checkAccountChange(username.getText(),old.getText()) &&
                !adm.checkAccountChange(username.getText(),old.getText()) &&
                !vis.checkAccountChange(username.getText(),old.getText()))
        ){
            one.setText("No user account");
        }else if(username.getText().length() == 0){
            one.setText("Please enter username");
        }else{
            one.setText("");
        }if(!newPass.getText().equals(confirm.getText())){
            four.setText("Password didn't match");
        }else{
            four.setText("");

        }if(old.getText().length() == 0){
            two.setText("Please enter old password");
        }else{
            two.setText("");
        }if(newPass.getText().length() == 0){
            three.setText("Please enter new password");
        }else{
            three.setText("");
        }if(confirm.getText().length() == 0){
            four.setText("Please enter confirm password");
        }else{
            four.setText("");
        }

    }public void handleBackOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
}
