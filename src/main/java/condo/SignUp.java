package condo;

import controller.personcontroller.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import person.Visitor;
import controller.roomcontroller.RoomDataSource;
import controller.roomcontroller.RoomFileDataSource;
import controller.roomcontroller.RoomManagement;

import java.io.IOException;
import java.util.Optional;

public class SignUp {
    @FXML
    private ComboBox combo,combo1,combo2;
    @FXML
    private Label errLabel;
    @FXML
    private TextField userField,nameField,phoneField,passField;
    @FXML Label one,two,three,four,five,six,seven;

    private UserVisitorDataSource addVis;
    private AccountVisitor vis;
    private Visitor a;

    private RoomDataSource addRm;
    private RoomManagement rm;


    private UserDataSource addAcc;
    private Account acc;

    private UserAdminDataSource addAdm;
    private AccountAdmin adm;
    private String user;
    public void setAccount(String user) {
        this.user = user;
    }

    @FXML private void initialize(){
        combo.getItems().removeAll(combo.getItems());
        combo.getItems().add("A bedroom.");
        combo.getItems().add("Two bedrooms.");
        combo.getSelectionModel().select("A bedroom.");

        for(int i = 1;10>=i;i++){
            combo1.getItems().add(Integer.toString(i));
        }

        addAcc = new AccountFileDataSource("Data","account.csv");
        acc = addAcc.getAccountData();

        addAdm = new AccountAdminFileDataSource("Data","accountAdmin.csv");
        adm = addAdm.getAccountData();

        addVis = new AccountVisitorFileDataSource("Data","accountVisitor.csv");
        vis = addVis.getAccountData();

        addRm = new RoomFileDataSource("Data","room.csv");
        rm = addRm.getRoomData();

    }
    @FXML
    public void handleComboOnAction(javafx.event.ActionEvent event) throws IOException {

        if(combo.getValue().toString().equals("A bedroom.")){
            combo2.getItems().removeAll(combo2.getItems());

            int num = Integer.parseInt(combo1.getValue().toString());
            for(int i = 1;5>=i;i++){
                int a = num*100+i;
                combo2.getItems().add(Integer.toString(a));
            }
        }else if(combo.getValue().toString().equals("Two bedrooms.")){
            if(combo1.getValue() == null){
                combo1.getSelectionModel().select("1");
            }
            combo2.getItems().removeAll(combo2.getItems());

            int num = Integer.parseInt(combo1.getValue().toString());
            for(int i = 6;10>=i;i++){
                int a = num*100+i;
                combo2.getItems().add(Integer.toString(a));
            }


        }
    }@FXML
    public void handleCombo1OnAction(javafx.event.ActionEvent event) throws IOException {

        combo2.getItems().removeAll(combo2.getItems());
        if(combo.getValue().toString().equals("A bedroom.")){
            combo2.getItems().removeAll(combo.getItems());

            int num = Integer.parseInt(combo1.getValue().toString());
            for(int i = 1;5>=i;i++){
                int a = num*100+i;
                combo2.getItems().add(Integer.toString(a));
            }
        }else if(combo.getValue().toString().equals("Two bedrooms.")){
            combo2.getItems().removeAll(combo2.getItems());
            int num = Integer.parseInt(combo1.getValue().toString());
            for(int i = 6;10>=i;i++){
                int a = num*100+i;
                combo2.getItems().add(Integer.toString(a));
            }
        }
    }
    @FXML
    public void handleFinishBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        if(combo1.getValue() != null && combo2.getValue() != null){
            if(vis.checkUser(userField.getText()) && acc.checkUser(userField.getText()) && adm.checkUser(userField.getText())
                    && rm.checkStatus(combo1.getValue().toString(),combo2.getValue().toString())
                    && userField.getText().length()>0
                    && passField.getText().length()>0
                    && phoneField.getText().length()>0
                    && nameField.getText().length()>0
            ){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirm to apply");
                alert.setContentText("Are you sure you want to apply?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){
                    a = new Visitor(nameField.getText(),userField.getText(),passField.getText(),phoneField.getText(),combo.getValue().toString()
                            ,combo1.getValue().toString(),combo2.getValue().toString());
                    vis.addAccount(a);
                    addVis.setAccountData(vis);
                    rm.changeStatus(combo1.getValue().toString(),combo2.getValue().toString());
                    addRm.setRoomData(rm);
                    Button b = (Button) event.getSource();
                    Stage stage = (Stage) b.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/data_resident.fxml"));
                    stage.setScene(new Scene(loader.load(), 800, 600));
                    DataResident dw = loader.getController();
                    dw.setAccount(user);
                    stage.show();
                } else {
                    alert.close();
                }

            }
        }if(!acc.checkUser(userField.getText()) || !adm.checkUser(userField.getText()) || !vis.checkUser(userField.getText())){
            one.setText("Username has been used.");
        }else if(userField.getText().length()==0){
            one.setText("Please enter username.");
        }else{
            one.setText("");

        }if(passField.getText().length()==0){
            two.setText("Please enter password.");
        }else{
            two.setText("");
        }if(nameField.getText().length()==0){
            three.setText("Please enter name.");
        }else{
            three.setText("");
        }if(phoneField.getText().length()==0){
            four.setText("Please enter phone number.");
        }else{
            four.setText("");
        }if(combo1.getValue() == null){
            six.setText("Please choose.");
        }else{
            six.setText("");
        }if(combo2.getValue()==null){
            seven.setText("Please Choose.");
        }else if(combo2.getValue()!=null){
            seven.setText("");
            if(!rm.checkStatus(combo1.getValue().toString(),combo2.getValue().toString())){
                seven.setText("Already owned.");
            }else if(rm.checkStatus(combo1.getValue().toString(),combo2.getValue().toString())){
                seven.setText("");
            }
        }



    }
    @FXML
    public void handleBackBtnOnAction(javafx.event.ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/data_resident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        DataResident dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }
}
