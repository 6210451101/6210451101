package condo;

import controller.personcontroller.Account;
import controller.personcontroller.AccountVisitor;
import controller.personcontroller.AccountVisitorFileDataSource;
import controller.personcontroller.UserVisitorDataSource;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import controller.roomcontroller.RoomDataSource;
import controller.roomcontroller.RoomFileDataSource;
import controller.roomcontroller.RoomManagement;

import java.io.IOException;
import java.util.Optional;

public class RemoveResident {
    @FXML private ComboBox roomCombo;
    @FXML private Label label;
    @FXML private Button backBtn,removeBtn;

    private RoomDataSource addRm;
    private RoomManagement rm;

    private AccountVisitor vis;
    private UserVisitorDataSource addVis;

    private Account acc;
    private String user;
    public void setAccount(String user) {
        this.user = user;
    }
    @FXML private void initialize(){
        addVis = new AccountVisitorFileDataSource("Data","accountVisitor.csv");
        vis = addVis.getAccountData();

        addRm = new RoomFileDataSource("Data","room.csv");
        rm = addRm.getRoomData();
        roomCombo.getItems().removeAll(roomCombo.getItems());

        for(int i = 0; i < vis.sizeOfArray();i++){
            roomCombo.getItems().add(vis.roomNum(i));
        }
    }@FXML
    public void handleBackBtnOnAction(javafx.event.ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/data_resident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        DataResident dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }@FXML
    public void handleRemoveBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        if(roomCombo.getValue() != null){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to remove account");
            alert.setContentText("Are you sure you want to remove the account?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){

                vis.removeAccount(roomCombo.getValue().toString());
                addVis.setAccountData(vis);
                rm.changeStatusRemove(roomCombo.getValue().toString());
                addRm.setRoomData(rm);
                roomCombo.getItems().removeAll(roomCombo.getItems());
                label.setText("Account removed.");
                for(int i = 0; i < vis.sizeOfArray();i++){
                    roomCombo.getItems().add(vis.roomNum(i));
                }
            } else {
                alert.close();
            }
        }


    }
}
