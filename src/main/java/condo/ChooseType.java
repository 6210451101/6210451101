package condo;

import controller.personcontroller.Account;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class ChooseType {
    @FXML
    Button postBtn, documentBtn,suppliesBtn,backBtn;
    @FXML
    private Account acc;
    private String user;
    public void setAccount(String user) {
        this.user = user;
    }
    public void handleBackBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/im_ex_port.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ImExPort dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handlePostBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/letter.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddLetter dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleSuppliesBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/supplies.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddSupplies dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleDocumentBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/document.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        AddDocument dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }

}
