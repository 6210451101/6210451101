package condo;

import controller.importexportcontroller.ImportExportManager;
import controller.importexportcontroller.MailManager;
import controller.importexportcontroller.ImportExportDataSource;
import controller.importexportcontroller.ImportExportFileDataSource;
import importexport.ImportExportHistory;
import mail.Letter;
import controller.personcontroller.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import controller.mailcontroller.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

public class Export {
    private AllMailDataSource addLet,addDoc,addSup;

    private ImportExportDataSource addHis;
    private ImportExportManager his;

    private UserDataSource addAcc;
    private Account acc;
    private String user;
    public void setAccount(String user) {
        this.user = user;
    }
    private MailManager m,doc,sup;
    @FXML
    private Label one,two,three;
    @FXML
    private TableView letterTable,documentTable,suppliesTable;
    @FXML private Button backBtn,finishBtn,exportLetter,exportDocument,exportSupplies;
    @FXML private ComboBox roomLetter,roomDocument,roomSupplies;
    @FXML public void initialize(){
        addAcc = new AccountFileDataSource("Data","account.csv");
        acc = addAcc.getAccountData();

        addHis = new ImportExportFileDataSource("Data","histories.csv");
        his = addHis.getAccountData();

        addLet = new AllMailFileDataSource("Data","letter.csv","letter");
        m = addLet.getAccountData();

        addDoc = new AllMailFileDataSource("Data","document.csv","document");
        doc = addDoc.getAccountData();

        addSup = new AllMailFileDataSource("Data","supplies.csv","supplies");
        sup = addSup.getAccountData();
        roomLetter.getItems().removeAll(roomLetter.getItems());
        for(int i = 0; i < m.sizeOfArray();i++){
            int k = 0;
            for(int j = 0;j<i;j++){
                if(m.roomNum(i).equals(m.roomNum(j))){
                    k++;
                }
            }
            if(k==0){
                roomLetter.getItems().add(m.roomNum(i));
            }
        }
        roomDocument.getItems().removeAll(roomDocument.getItems());
        for(int i = 0; i < doc.sizeOfArray();i++){
            int k = 0;
            for(int j = 0;j<i;j++){
                if(doc.roomNum(i).equals(doc.roomNum(j))){
                    k++;
                }
            }
            if(k==0){
                roomDocument.getItems().add(doc.roomNum(i));
            }
        }
        roomSupplies.getItems().removeAll(roomSupplies.getItems());
        for(int i = 0; i < sup.sizeOfArray();i++){
            int k = 0;
            for(int j = 0;j<i;j++){
                if(sup.roomNum(i).equals(sup.roomNum(j))){
                    k++;
                }
            }
            if(k==0){
                roomSupplies.getItems().add(sup.roomNum(i));
            }
        }
        letterTable.setDisable(true);
        letterTable.setStyle("-fx-opacity: 1");
        documentTable.setDisable(true);
        documentTable.setStyle("-fx-opacity: 1");
        suppliesTable.setDisable(true);
        suppliesTable.setStyle("-fx-opacity: 1");
        showAccountData();
        showAccountData1();
        showAccountData2();
    }
    private void showAccountData() {
        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(m.toList());
        letterTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            letterTable.getColumns().add(col);
        }

    }
    private void showAccountData1() {
        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(doc.toList());
        documentTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        configs.add(new StringConfiguration("title:Important", "field:important"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            documentTable.getColumns().add(col);
        }
    }
    private void showAccountData2() {
        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(sup.toList());
        suppliesTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        configs.add(new StringConfiguration("title:Tracking Number", "field:trackNum"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            suppliesTable.getColumns().add(col);
        }
    }public void handleExportLetterOnAction(javafx.event.ActionEvent event) throws IOException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        try{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to export letter");
            alert.setContentText("Are you sure you want to export the letter?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                ImportExportHistory a = new ImportExportHistory(roomLetter.getValue().toString(),"Letter.","Exported.",acc.name(user),dtf.format(now));
                his.addAccount(a);
                addHis.setAccountData(his);
                m.removeAccount(roomLetter.getValue().toString());
                addLet.setAccountData(m,new LetterDataForSetFile());

                roomLetter.getItems().removeAll(roomLetter.getItems());
                for(int i = 0; i < m.sizeOfArray();i++){
                    int k = 0;
                    for(int j = 0;j<i;j++){
                        if(m.roomNum(i).equals(m.roomNum(j))){
                            k++;
                        }
                    }
                    if(k==0){
                        roomLetter.getItems().add(m.roomNum(i));
                    }
                }
                letterTable.getColumns().clear();
                showAccountData();
            } else {
                alert.close();
            }
        }catch (NullPointerException e){
            one.setText("Please choose the room");
        }
    }public void handleExportDocumentOnAction(javafx.event.ActionEvent event) throws IOException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        try{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to export document");
            alert.setContentText("Are you sure you want to export the document?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                ImportExportHistory a = new ImportExportHistory(roomDocument.getValue().toString(),"Document.","Exported.",acc.name(user),dtf.format(now));
                his.addAccount(a);
                addHis.setAccountData(his);
                doc.removeAccount(roomDocument.getValue().toString());
                addDoc.setAccountData(doc,new DocumentDataForSetFile());
                roomDocument.getItems().removeAll(roomDocument.getItems());
                for(int i = 0; i < doc.sizeOfArray();i++){
                    int k = 0;
                    for(int j = 0;j<i;j++){
                        if(doc.roomNum(i).equals(doc.roomNum(j))){
                            k++;
                        }
                    }
                    if(k==0){
                        roomDocument.getItems().add(doc.roomNum(i));
                    }
                }
                documentTable.getColumns().clear();
                showAccountData1();
            } else {
                alert.close();
            }



        }catch (NullPointerException e){
            two.setText("Please choose the room");
        }
    }
    public void handleExportSuppliesOnAction(javafx.event.ActionEvent event) throws IOException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to export supplies");
            alert.setContentText("Are you sure you want to export the supplies?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                ImportExportHistory a = new ImportExportHistory(roomSupplies.getValue().toString(),"Document.","Exported.",acc.name(user),dtf.format(now));
                his.addAccount(a);
                addHis.setAccountData(his);
                sup.removeAccount(roomSupplies.getValue().toString());
                addSup.setAccountData(sup,new SuppliesDataForSetFile());
                roomSupplies.getItems().removeAll(roomSupplies.getItems());
                for(int i = 0; i < sup.sizeOfArray();i++){
                    int k = 0;
                    for(int j = 0;j<i;j++){
                        if(sup.roomNum(i).equals(sup.roomNum(j))){
                            k++;
                        }
                    }
                    if(k==0){
                        roomSupplies.getItems().add(sup.roomNum(i));
                    }
                }
                suppliesTable.getColumns().clear();
                showAccountData2();
            } else {
                alert.close();
            }




        }catch (NullPointerException e){
            three.setText("Please choose the room");
        }
    }
    public void handleBackBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/im_ex_port.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ImExPort dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleFinishBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
}