package condo;

import controller.personcontroller.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import person.AdminClass;
import room.*;
import controller.roomcontroller.RoomDataSource;
import controller.roomcontroller.RoomFileDataSource;
import controller.roomcontroller.RoomManagement;

public class Login {
    @FXML
    private TextField userField;
    @FXML
    private PasswordField passwordField;

    private UserDataSource addAcc;
    private Account acc;

    private AdminClass admin;
    private UserAdminDataSource addAdm;
    private AccountAdmin adm;

    private RoomDataSource addRm;
    private RoomManagement rm;
    private TypeRoom typeRoom;


    private UserVisitorDataSource addVis;
    private AccountVisitor vis;

    private String user;
    @FXML private Label one;
    @FXML
    public void initialize() {
        addAcc = new AccountFileDataSource("Data","account.csv");
        acc = addAcc.getAccountData();

        addAdm = new AccountAdminFileDataSource("Data","accountAdmin.csv");
        adm = addAdm.getAccountData();

        addRm = new RoomFileDataSource("Data","room.csv");
        rm = addRm.getRoomData();

        addVis = new AccountVisitorFileDataSource("Data","accountVisitor.csv");
        vis = addVis.getAccountData();

        if(rm.checkFile()){
            for(int i = 100;1000>=i;i+=100){
                for(int j = 1;5>=j;j++) {
                    String floor, room;
                    floor = Integer.toString(i / 100);
                    room = Integer.toString(i + j);
                    typeRoom = new TypeRoom("One bed room",floor, room, "No owner.");
                    rm.addRoom(typeRoom);
                    addRm.setRoomData(rm);
                }
                for(int j = 6;10>=j;j++){
                    String floor2,room2;
                    floor2 = Integer.toString(i/100);
                    room2 = Integer.toString(i+j);
                    typeRoom = new TypeRoom("Two bed rooms",floor2,room2,"No owner.");
                    rm.addRoom(typeRoom);
                    addRm.setRoomData(rm);
                }
            }
        }if(adm.checkFile()){
            admin = new AdminClass("Chaiyanon Klabkangwal","admin","1234");
            adm.addAccount(admin);
            addAdm.setAccountData(adm);
        }
    }
    @FXML
    public void handleLoginBtnOnAction(javafx.event.ActionEvent event) throws IOException {

        if (acc.checkAccount(userField.getText(), passwordField.getText()) ) {
            if (acc.getStatus(userField.getText(), passwordField.getText()).equals("Available")) {
                System.out.println(acc.getCurrentAccount());
                System.out.println(userField.getText());
                addAcc.setAccountData(acc);
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/data_resident.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                DataResident dw = loader.getController();
//            dw.setAcc(acc);
                dw.setAccount(userField.getText());
                stage.show();
            }else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Log in error");
                alert.setContentText("Ooops, This account suspended");
                alert.showAndWait();
            }
        }else if(adm.checkAccount(userField.getText(), passwordField.getText())){
            System.out.println(adm.toString());
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/all_staff.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            stage.show();
        }else if(vis.checkAccount(userField.getText(),passwordField.getText())){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
            stage.setScene(new Scene(loader.load(), 800, 600));
            Resident dw = loader.getController();
            dw.setAccount(vis);

            stage.show();
        }else{
            one.setText("No user account.");
        }

    }

    @FXML
    public void handleCrtBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/profile.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
    @FXML
    public void handleChangeOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/change_password.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }@FXML
    public void handleManualOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manual.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
}