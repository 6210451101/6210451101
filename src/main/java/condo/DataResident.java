package condo;

import controller.personcontroller.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.stage.Stage;
import room.*;
import controller.roomcontroller.RoomDataSource;
import controller.roomcontroller.RoomFileDataSource;
import controller.roomcontroller.RoomManagement;

import java.io.IOException;
import java.util.ArrayList;

public class DataResident {
    @FXML Button backBtn,nextBtn,regisBtn;
    @FXML
    TextField roomField,floorField;
    @FXML
    Label label,nameAccount;
    @FXML
    ImageView imageView;
    @FXML
    TableView column;
    private Account acc;
    private RoomDataSource addRm;
    private RoomManagement rm;

    private AccountVisitor vis;
    private UserVisitorDataSource addVis;

    String user;

    private UserDataSource addAcc;
    public void setAccount(String user) {
        this.user = user;
        showImage();
    }
    @FXML
    public void initialize() {
        addRm = new RoomFileDataSource("Data","room.csv");
        rm = addRm.getRoomData();


        addAcc = new AccountFileDataSource("Data","account.csv");
        acc = addAcc.getAccountData();

        addVis = new AccountVisitorFileDataSource("Data","accountVisitor.csv");
        vis = addVis.getAccountData();
        showAccountData();

    }
    private void showAccountData() {
        ObservableList<TypeRoom> accountObservableList = FXCollections.observableArrayList(rm.toList());
        column.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Room Type", "field:typeRoom"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room Number", "field:roomNumber"));
        configs.add(new StringConfiguration("title:Status", "field:status"));
        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            column.getColumns().add(col);
        }

    }

    @FXML
    public void handleBackBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        System.out.println(acc.getImage(user));
        System.out.println(user);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }public void handleNextBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/im_ex_port.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ImExPort dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleRegisterBtnOnAction(javafx.event.ActionEvent event) throws IOException {
//        System.out.println(user);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sign_up.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        SignUp dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleRemoveBtnOnAction(javafx.event.ActionEvent event) throws IOException {
//        System.out.println(user);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/remove_resident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        RemoveResident dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }

    private void showImage(){
        Image image = new Image(acc.getImage(user));
        imageView.setFitHeight(70);
        imageView.setPreserveRatio(true);
        imageView.setImage(image);
        nameAccount.setText(acc.name(user)+"\n"+user);

    }
}
