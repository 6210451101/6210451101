package condo;

import controller.importexportcontroller.ImportExportManager;
import controller.importexportcontroller.ImportExportDataSource;
import controller.importexportcontroller.ImportExportFileDataSource;
import importexport.ImportExportHistory;
import controller.personcontroller.StringConfiguration;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class History {
    @FXML
    private TableView table;
    private ImportExportDataSource addHis;
    private ImportExportManager his;
    private String user;

    public void setAccount(String user) {
        this.user = user;
    }

    @FXML
    public void initialize() {
        addHis = new ImportExportFileDataSource("Data", "histories.csv");
        his = addHis.getAccountData();

        showAccountData();
    }

    private void showAccountData() {
        ObservableList<ImportExportHistory> accountObservableList = FXCollections.observableArrayList(his.toList());
        table.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Status", "field:status"));
        configs.add(new StringConfiguration("title:Recipient Room", "field:room"));
        configs.add(new StringConfiguration("title:Type of mail", "field:typeMail"));
        configs.add(new StringConfiguration("title:Name staff", "field:nameStaff"));
        configs.add(new StringConfiguration("title:Time", "field:time"));
        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            table.getColumns().add(col);
        }

    }

    @FXML
    public void handleBackOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/im_ex_port.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ImExPort dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }

    @FXML
    public void handleClearOnAction(javafx.event.ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm to clear history");
        alert.setContentText("Are you sure you want to clear history?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            table.getColumns().clear();
            his.clearList();
            addHis.setAccountData(his);
            showAccountData();
        } else {
            alert.close();
        }

    }
}
