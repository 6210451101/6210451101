package condo;

import controller.personcontroller.Account;
import controller.personcontroller.AccountFileDataSource;
import controller.personcontroller.StringConfiguration;
import controller.personcontroller.UserDataSource;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import person.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class AllStaff{
    private UserDataSource dataSource;
    private Account acc;
    private User account;
    @FXML private ComboBox accCombo;
    private ObservableList<User> accountObservableList;
    @FXML private Button backBtn,nextBtn;
    @FXML private TableView<User> accountTable;
    @FXML private Label one;
//    private AccountAdmin adm;
    @FXML public void initialize(){
        dataSource = new AccountFileDataSource("Data", "account.csv");
        acc = dataSource.getAccountData();
        accCombo.getItems().removeAll(accCombo.getItems());
        for(int i=0;i<acc.getSize();i++){
            accCombo.getItems().add(acc.getAcc(i));
        }
        showAccountData();
    }
    private void showAccountData() {
        accountObservableList = FXCollections.observableArrayList(acc.toList());
        accountTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Status", "field:status"));
        configs.add(new StringConfiguration("title:Name", "field:name"));
        configs.add(new StringConfiguration("title:Username", "field:username"));
        configs.add(new StringConfiguration("title:Password", "field:password"));
        configs.add(new StringConfiguration("title:Last login time", "field:time"));


        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            accountTable.getColumns().add(col);
        }
    }
    @FXML public void handleCancelBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
    @FXML public void handleNextBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }@FXML public void handleSusBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        try{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to Release the suspension or Suspend");
            alert.setContentText("Are you sure you want to Release the suspension or Suspend?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                acc.changeStatus(accCombo.getValue().toString());
                dataSource.setAccountData(acc);
                showAccountData();
            } else {
                alert.close();
            }
        }catch (NullPointerException e){
            one.setText("Please choose the account");
        }
    }@FXML public void handleRemoveBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        try{
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm to remove account");
            alert.setContentText("Are you sure you want to remove this account");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                acc.removeAccount(accCombo.getValue().toString());
                dataSource.setAccountData(acc);
                accCombo.getItems().removeAll(accCombo.getItems());
                for(int i=0;i<acc.getSize();i++){
                    accCombo.getItems().add(acc.getAcc(i));
                }
                showAccountData();
            } else {
                alert.close();
            }
        }catch (NullPointerException e){
            one.setText("Please choose the account");
        }
    }

}
