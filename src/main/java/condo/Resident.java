package condo;

import mail.Letter;
import controller.personcontroller.AccountVisitor;
import controller.personcontroller.StringConfiguration;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import controller.mailcontroller.AllMailDataSource;
import controller.mailcontroller.AllMailFileDataSource;
import controller.importexportcontroller.MailManager;

import java.io.IOException;
import java.util.ArrayList;

public class Resident {
    private AllMailDataSource addLet,addDoc,addSup;
    @FXML
    Button finishBtn;
    @FXML
    TableView tableView;
    @FXML
    ComboBox typeCombo;
    @FXML
    ImageView imageView;
    @FXML
    Label label;
    private AccountVisitor vis;
    private MailManager m,doc,sup;

    public void setAccount(AccountVisitor vis) {
        this.vis = vis;
        showname();
    }

    @FXML
    public void initialize() {

        addLet = new AllMailFileDataSource("Data", "letter.csv","letter");
        m = addLet.getAccountData();

        addDoc = new AllMailFileDataSource("Data", "document.csv","document");
        doc = addDoc.getAccountData();

        addSup = new AllMailFileDataSource("Data", "supplies.csv","supplies");
        sup = addSup.getAccountData();
        typeCombo.getItems().removeAll(typeCombo.getItems());
        typeCombo.getItems().addAll("Letter", "Document", "Supplies");
        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                SelectedMail((mail.Letter) newValue);
            }
        });
    }
    @FXML public void SelectedMail(mail.Letter letter){
        imageView.setFitHeight(250);
        imageView.setPreserveRatio(true);
        imageView.setImage(new Image(letter.getImage()));
    }

    public void handleFinishBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }private void showAccountData() {
        tableView.getColumns().clear();
        ArrayList<mail.Letter> arrlist = new ArrayList<>(1000);
        for(int i = 0 ;i< m.sizeOfArray();i++){
            if(m.checkCurrent(i,vis.getCurrentAccount().getRoomNum()) != null){
                arrlist.add(m.checkCurrent(i,vis.getCurrentAccount().getRoomNum()));
            }else{
                continue;
            }
            tableView.getColumns().clear();
            ObservableList<mail.Letter> accountObservableList = FXCollections.observableArrayList(arrlist);
            tableView.setItems(accountObservableList);
            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
            configs.add(new StringConfiguration("title:Floor", "field:floor"));
            configs.add(new StringConfiguration("title:Room", "field:roomNum"));
            configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
            configs.add(new StringConfiguration("title:Width(cm)", "field:width"));
            configs.add(new StringConfiguration("title:Length(cm)", "field:length"));
            for (StringConfiguration conf: configs) {
                TableColumn col = new TableColumn(conf.get("title"));
                col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
                tableView.getColumns().add(col);
            }
        }

    }private void showAccountData1() {
        tableView.getColumns().clear();
        ArrayList<Letter> arrlist = new ArrayList<Letter>(1000);
        for(int i = 0 ;i< doc.sizeOfArray();i++){
            if(doc.checkCurrent(i,vis.getCurrentAccount().getRoomNum()) != null){
                arrlist.add(doc.checkCurrent(i,vis.getCurrentAccount().getRoomNum()));
            }else{
                continue;
            }
            tableView.getColumns().clear();
            ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(arrlist);
            tableView.setItems(accountObservableList);
            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
            configs.add(new StringConfiguration("title:Floor", "field:floor"));
            configs.add(new StringConfiguration("title:Room", "field:roomNum"));
            configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
            configs.add(new StringConfiguration("title:Important", "field:important"));
            configs.add(new StringConfiguration("title:Width(cm)", "field:width"));
            configs.add(new StringConfiguration("title:Length(cm)", "field:length"));
            for (StringConfiguration conf: configs) {
                TableColumn col = new TableColumn(conf.get("title"));
                col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
                tableView.getColumns().add(col);
            }
        }
    }private void showAccountData2() {
        tableView.getColumns().clear();
        ArrayList<Letter> arrlist = new ArrayList<Letter>(1000);
        for(int i = 0 ;i< sup.sizeOfArray();i++){
            if(sup.checkCurrent(i,vis.getCurrentAccount().getRoomNum()) != null){
                arrlist.add(sup.checkCurrent(i,vis.getCurrentAccount().getRoomNum()));
            }else{
                continue;
            }
            tableView.getColumns().clear();
            ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(arrlist);
            tableView.setItems(accountObservableList);
            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
            configs.add(new StringConfiguration("title:Floor", "field:floor"));
            configs.add(new StringConfiguration("title:Room", "field:roomNum"));
            configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
            configs.add(new StringConfiguration("title:Important", "field:important"));
            configs.add(new StringConfiguration("title:Width(cm)", "field:width"));
            configs.add(new StringConfiguration("title:Length(cm)", "field:length"));
            for (StringConfiguration conf: configs) {
                TableColumn col = new TableColumn(conf.get("title"));
                col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
                tableView.getColumns().add(col);
            }
        }
    }public void handleSearchOnAction(javafx.event.ActionEvent event) throws IOException {
        if(typeCombo.getValue().toString().equals("Letter")){
            showAccountData();
        }else if(typeCombo.getValue().toString().equals("Document")){
            showAccountData1();
        }else if(typeCombo.getValue().toString().equals("Supplies")){
            showAccountData2();
        }
    }public void showname(){
        label.setText(vis.getCurrentAccount().toString());
    }
}
