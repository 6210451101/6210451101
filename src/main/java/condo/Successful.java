package condo;

import mail.Letter;
import controller.personcontroller.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import controller.mailcontroller.AllMailDataSource;
import controller.mailcontroller.AllMailFileDataSource;
import controller.importexportcontroller.MailManager;

import java.io.IOException;
import java.util.ArrayList;


public class Successful {
    private UserVisitorDataSource addvis;
    private AccountVisitor vis;

    private AllMailDataSource addLet,addDoc,addSup;
    @FXML
    private TableView letterTable,documentTable,suppliesTable;
    private Account acc;
    private String user;
    private MailManager m,doc,sup;
    public void setAccount(String user) {
        this.user = user;
    }
    @FXML
    Button continueBtn,finishBtn;
    @FXML public void initialize(){
        addvis = new AccountVisitorFileDataSource("Data", "accountVisitor.csv");
        vis = addvis.getAccountData();

        addLet = new AllMailFileDataSource("Data","letter.csv","letter");
        m = addLet.getAccountData();

        addDoc = new AllMailFileDataSource("Data","document.csv","document");
        doc = addDoc.getAccountData();

        addSup = new AllMailFileDataSource("Data","supplies.csv","supplies");
        sup = addSup.getAccountData();

        showAccountData();
        showAccountData1();
        showAccountData2();
    }
    private void showAccountData() {
        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(m.toList());
        letterTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            letterTable.getColumns().add(col);
        }

    }
    private void showAccountData1() {
        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(doc.toList());
        documentTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        configs.add(new StringConfiguration("title:Important", "field:important"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            documentTable.getColumns().add(col);
        }
    }
    private void showAccountData2() {
        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(sup.toList());
        suppliesTable.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        configs.add(new StringConfiguration("title:Tracking Number", "field:trackNum"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            suppliesTable.getColumns().add(col);
        }
    }
    public void handleContinueBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        System.out.println(user);

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/im_ex_port.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ImExPort dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleFinishBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login_page.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        stage.show();
    }
}
