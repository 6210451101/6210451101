package condo;

import controller.importexportcontroller.ImportExportManager;
import controller.importexportcontroller.MailManager;
import controller.importexportcontroller.ImportExportDataSource;
import controller.importexportcontroller.ImportExportFileDataSource;
import importexport.ImportExportHistory;
import controller.personcontroller.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mail.*;
import controller.mailcontroller.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class AddDocument {
    private UserVisitorDataSource addVis;
    private AccountVisitor vis;

    private AllMailDataSource addDoc;

    private ImportExportDataSource addHis;
    private ImportExportManager his;
    @FXML
    TextField recipField,senderField,widthField,lengthField,addressField,newField;
    @FXML
    Button backBtn,nextBtn,browse;
    @FXML
    ComboBox floor,room,importance;
    @FXML
    Label one,two,three,four,five,six,seven;
    @FXML
    ImageView documentImage;
    private UserDataSource addAcc;
    private Account acc;
    private String user;

    private MailManager doc;

    public void setAccount(String user) {
        this.user = user;
    }
    @FXML private void initialize(){
        addressField.setDisable(true);
        addressField.setStyle("-fx-opacity: 1");
        addDoc = new AllMailFileDataSource("Data","document.csv","document");
        doc = addDoc.getAccountData();

        addVis = new AccountVisitorFileDataSource("Data","accountVisitor.csv");
        vis = addVis.getAccountData();

        addHis = new ImportExportFileDataSource("Data","histories.csv");
        his = addHis.getAccountData();

        addAcc = new AccountFileDataSource("Data","account.csv");
        acc = addAcc.getAccountData();
        importance.getItems().add("Express documents.");
        importance.getItems().add("Confidential documents.");

        for(int i = 1;10>=i;i++){
            floor.getItems().add(Integer.toString(i));
        }
        floor.getSelectionModel().select("1");
        int num = Integer.parseInt(floor.getValue().toString());
        for(int i = 1;10>=i;i++){
            int a = num*100+i;
            room.getItems().add(Integer.toString(a));
        }
        room.getSelectionModel().select("101");


    }
    @FXML
    public void handleBrowseBtnOnAction(ActionEvent event){
        final FileChooser chooser = new FileChooser();
//        Stage stage = (Stage) put.getScene().getWindow();

        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File file = chooser.showOpenDialog(browse.getScene().getWindow());

        if (file != null){
            addressField.setText(file.getPath());
            try {
                // CREATE FOLDER IF NOT EXIST
                File destDir = new File("images");
                destDir.mkdirs();

                // RENAME FILE
                String[] fileSplit = file.getName().split("\\.");
                String filename = LocalDate.now()+"_"+System.currentTimeMillis()+"."+fileSplit[fileSplit.length - 1];
                Path target = FileSystems.getDefault().getPath(destDir.getAbsolutePath()+System.getProperty("file.separator")+filename);
                // COPY WITH FLAG REPLACE FILE IF FILE IS EXIST
                File f =target.toFile();
                Files.copy(file.toPath(), target, StandardCopyOption.REPLACE_EXISTING );
                // SET NEW FILE PATH TO IMAGE
                documentImage.setImage(new Image(target.toUri().toString()));

                newField.setText(f.toURI().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @FXML
    public void handleFloorOnAction(javafx.event.ActionEvent event) throws IOException {
        room.getItems().removeAll(room.getItems());
        int num = Integer.parseInt(floor.getValue().toString());
        for(int i = 1;10>=i;i++){
            int a = num*100+i;
            room.getItems().add(Integer.toString(a));
        }
    }
    @FXML
    public void handleBackBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ChooseType dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleNextBtnOnAction(javafx.event.ActionEvent event) throws IOException {

        if(floor.getValue() != null && room.getValue() != null) {
            if (vis.checkRoom(floor.getValue().toString(), room.getValue().toString())
                    && recipField.getText().length() > 0
                    && senderField.getText().length() > 0
                    && widthField.getText().length() > 0
                    && lengthField.getText().length() > 0
                    && addressField.getText().length() > 0
                    && newField.getText().length() > 0
                    && importance.getValue().toString().length() > 0
            ) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirm to import document");
                alert.setContentText("Are you sure you want to import document?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    Letter d = new Document(recipField.getText(), floor.getValue().toString(),
                            room.getValue().toString(), senderField.getText(), importance.getValue().toString(),
                            widthField.getText(), lengthField.getText(), newField.getText());
                    doc.addAccount(d);
                    addDoc.setAccountData(doc,new DocumentDataForSetFile());
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                    LocalDateTime now = LocalDateTime.now();
                    ImportExportHistory a = new ImportExportHistory(room.getValue().toString(),"Document.","Imported.",acc.name(user),dtf.format(now));
                    his.addAccount(a);
                    addHis.setAccountData(his);
                    Button b = (Button) event.getSource();
                    Stage stage = (Stage) b.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/successful.fxml"));
                    stage.setScene(new Scene(loader.load(), 800, 600));
                    Successful dw = loader.getController();
                    dw.setAccount(user);
                    stage.show();
                } else {
                    alert.close();
                }

            }
        }
        if(recipField.getText().length() == 0){
            one.setText("Please enter recipient name.");
        }else{
            one.setText("");
        }if(room.getValue() == null){
            three.setText("Choose room.");
            three.setStyle("-fx-text-fill: Red");
        }else if (room.getValue() != null){
            if(!vis.checkRoom(floor.getValue().toString(),room.getValue().toString())){
                three.setText("No owner.");
                three.setStyle("-fx-text-fill: Red");
            }else{
                three.setText("Room.");
                three.setStyle("-fx-text-fill: Black");
            }
        }
        else{
            three.setText("Room.");
            three.setStyle("-fx-text-fill: White");

        }if(senderField.getText().length() ==0){
            four.setText("Please enter sender information.");
        }else{
            four.setText("");
        }if(widthField.getText().length() ==0 || lengthField.getText().length() == 0){
            seven.setText("Size error.");
        }else {
            seven.setText("");
        }if(documentImage.getImage() == null){
            six.setText("Please put a image.");
        }else{
            six.setText("");
        }if(importance.getValue() == null){
            five.setText("Choose the importance of the document.");
            five.setStyle("-fx-text-fill: Red");
        }else{
            five.setText("Important.");
            five.setStyle("-fx-text-fill: White");
        }
    }
}
