package condo;

import mail.Letter;
import controller.personcontroller.StringConfiguration;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import controller.mailcontroller.AllMailDataSource;
import controller.mailcontroller.AllMailFileDataSource;
import controller.importexportcontroller.MailManager;

import java.io.IOException;
import java.util.ArrayList;

public class ImExPort {
    private AllMailDataSource addLet,addDoc,addSup;
    @FXML private ComboBox typeCombo;

    @FXML private ImageView imageView;
    @FXML private TableView tableView;
    @FXML
    Button imBtn,exBtn,backBtn;
    private MailManager l,d,s;
    private String user;
    public void setAccount(String user ){
        this.user = user;
    }
    @FXML public void initialize(){

        addLet = new AllMailFileDataSource("Data","letter.csv","letter");
        l = addLet.getAccountData();

        addDoc = new AllMailFileDataSource("Data","document.csv","document");
        d = addDoc.getAccountData();
        addSup = new AllMailFileDataSource("Data","supplies.csv","supplies");
        s = addSup.getAccountData();
        typeCombo.getItems().removeAll(typeCombo.getItems());
        typeCombo.getItems().addAll("Letter","Document","Supplies");
        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                SelectedMail((Letter) newValue);
            }
        });
    }private void showAccountData() {
        tableView.getColumns().clear();
        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(l.toList());
        tableView.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        configs.add(new StringConfiguration("title:Width(cm)", "field:width"));
        configs.add(new StringConfiguration("title:Length(cm)", "field:length"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            tableView.getColumns().add(col);
        }
    }private void showAccountData1() {
        tableView.getColumns().clear();

        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(d.toList());
        tableView.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        configs.add(new StringConfiguration("title:Important", "field:important"));
        configs.add(new StringConfiguration("title:Width(cm)", "field:width"));
        configs.add(new StringConfiguration("title:Length(cm)", "field:length"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            tableView.getColumns().add(col);
        }
    }
    private void showAccountData2() {
        tableView.getColumns().clear();

        ObservableList<Letter> accountObservableList = FXCollections.observableArrayList(s.toList());
        tableView.setItems(accountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Recipient Name", "field:recipName"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Room", "field:roomNum"));
        configs.add(new StringConfiguration("title:Sender Information", "field:sender"));
        configs.add(new StringConfiguration("title:Tracking Number", "field:trackNum"));
        configs.add(new StringConfiguration("title:Width(cm)", "field:width"));
        configs.add(new StringConfiguration("title:Length(cm)", "field:length"));
        configs.add(new StringConfiguration("title:Height(cm)", "field:height"));
        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            tableView.getColumns().add(col);
        }
    }
    @FXML public void SelectedMail(Letter letter){
        imageView.setFitHeight(250);
        imageView.setPreserveRatio(true);
        imageView.setImage(new Image(letter.getImage()));
    }public void handleImBtnOnAction(javafx.event.ActionEvent event) throws IOException {
//        System.out.println(user);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/choose_type.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        ChooseType dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleBackBtnOnAction(javafx.event.ActionEvent event) throws IOException {
//        System.out.println(user);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/data_resident.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        DataResident dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleExBtnOnAction(javafx.event.ActionEvent event) throws IOException {
//        System.out.println(user);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/export.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        Export dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }public void handleHisBtnOnAction(javafx.event.ActionEvent event) throws IOException {
//        System.out.println(user);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/history.fxml"));
        stage.setScene(new Scene(loader.load(), 800, 600));
        History dw = loader.getController();
        dw.setAccount(user);
        stage.show();
    }
    public void handleSearchBtnOnAction(javafx.event.ActionEvent event) throws IOException {
        if(typeCombo.getValue().toString().equals("Letter")){
            showAccountData();
        }else if(typeCombo.getValue().toString().equals("Document")){
            showAccountData1();
        }else if(typeCombo.getValue().toString().equals("Supplies")){
            showAccountData2();
        }

    }
}
