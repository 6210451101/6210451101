package controller.mailcontroller;

import controller.importexportcontroller.MailManager;
import mail.Document;
import mail.Letter;
import mail.Supplies;

import java.io.*;

public class AllMailFileDataSource implements AllMailDataSource {
    private String fileDirectoryName;
    private String fileName;
    private String type;
    private MailManager mail;

    public AllMailFileDataSource(String fileDirectoryName, String fileName, String type) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        this.type = type;
        checkFileIsExisted();
    }
    private void checkFileIsExisted(){
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            if(type.equals("letter")){
                Letter m = new Letter(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim());
                mail.addAccount(m);
            }else if (type.equals("document")){
                Document m = new Document(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim(),data[7].trim());
                mail.addAccount(m);
            }else{
                Supplies m = new Supplies(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim(),data[7].trim(),data[8].trim(),data[9].trim());
                mail.addAccount(m);
            }
        }
        reader.close();
    }

    @Override
    public MailManager getAccountData() {
        try {
            mail = new MailManager();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return mail;
    }

    @Override
    public void setAccountData(MailManager mail,SetToFile s) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {

            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Letter let: mail.toList()) {

                String line = s.getData(let);

                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
