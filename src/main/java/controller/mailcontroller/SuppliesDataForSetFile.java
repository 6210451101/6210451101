package controller.mailcontroller;

import mail.Letter;
import mail.Supplies;

public class SuppliesDataForSetFile implements SetToFile{
    @Override
    public String getData(Letter mail) {
        return mail.getRecipName() + ","
                + mail.getFloor() + ","
                + mail.getRoomNum() + ","
                + mail.getSender()+","
                + ((Supplies)mail).getCourier()+","
                + ((Supplies)mail).getTrackNum()+","
                +mail.getWidth()+ ","
                +mail.getLength() + ","
                + ((Supplies)mail).getHeight()+","
                +mail.getImage();
    }
}
