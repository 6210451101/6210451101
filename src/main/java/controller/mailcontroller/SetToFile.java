package controller.mailcontroller;

import mail.Letter;

public interface SetToFile {
    String getData(Letter mail);
}
