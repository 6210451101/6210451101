package controller.mailcontroller;

import mail.Letter;

public class LetterDataForSetFile implements SetToFile{

    @Override
    public String getData(Letter mail) {
        return mail.getRecipName() + ","
                + mail.getFloor() + ","
                + mail.getRoomNum() + ","
                + mail.getSender()+","
                +mail.getWidth()+ ","
                +mail.getLength() + ","
                +mail.getImage();
    }
}