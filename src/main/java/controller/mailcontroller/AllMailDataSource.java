package controller.mailcontroller;

import controller.importexportcontroller.MailManager;

public interface AllMailDataSource {
    MailManager getAccountData();
    void setAccountData(MailManager letter,SetToFile s);
}