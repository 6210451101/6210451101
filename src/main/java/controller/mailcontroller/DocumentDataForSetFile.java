package controller.mailcontroller;

import mail.Document;
import mail.Letter;

public class DocumentDataForSetFile implements SetToFile {
    @Override
    public String getData(Letter mail) {
        return mail.getRecipName() + ","
                + mail.getFloor() + ","
                + mail.getRoomNum() + ","
                + mail.getSender()+","
                + ((Document)mail).getImportant() + ","
                + mail.getWidth()+ ","
                + mail.getLength() + ","
                + mail.getImage() ;
    }
}
