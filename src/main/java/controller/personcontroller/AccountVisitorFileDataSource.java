package controller.personcontroller;

import person.Visitor;

import java.io.*;

public class AccountVisitorFileDataSource implements UserVisitorDataSource{

    private String fileDirectoryName;
    private String fileName;
    private AccountVisitor accountVi;

    public AccountVisitorFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }
    private void checkFileIsExisted(){
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Visitor acc = new Visitor(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim());
            accountVi.addAccount(acc);
        }
        reader.close();
    }
    @Override
    public AccountVisitor getAccountData() {
        try {
            accountVi = new AccountVisitor();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return accountVi;
    }

    @Override
    public void setAccountData(AccountVisitor account) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {

            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Visitor acc: accountVi.toList()) {

                String line = acc.getName() + ","
                        + acc.getUsername() + ","
                        + acc.getPassword() + ","
                        + acc.getPhone() + ","
                        + acc.getType() + ","
                        + acc.getFloor() + ","
                        + acc.getRoomNum();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
