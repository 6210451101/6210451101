package controller.personcontroller;

public interface DataAccount {
    boolean checkAccount(String username,String password);
    int checkForChange(String username, String password);

}
