package controller.personcontroller;

import person.Visitor;

import java.util.ArrayList;

public class AccountVisitor implements DataAccount {
    private ArrayList<Visitor> accountVi;
    private Visitor currentAccount;
    private UserDataSource addVis;
    public AccountVisitor() {accountVi= new ArrayList<>();}
    public void addAccount(Visitor acc){
        accountVi.add(acc);
    }
    public Visitor getCurrentAccount() {
        return currentAccount;
    }
    public int sizeOfArray(){
        return accountVi.size();
    }
    @Override
    public String toString() {
        return accountVi.toString();
    }
    @Override
    public boolean checkAccount(String username, String password) {
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getUsername().equals(username) && accountVi.get(i).getPassword().equals(password)) {
                currentAccount = accountVi.get(i);
                return true;
            }
        }
        currentAccount = null;
        return false;
    }public String roomNum(int i){
        return accountVi.get(i).getRoomNum();
    }public void removeAccount(String room){
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getRoomNum().equals(room)) {
                accountVi.remove(i);
            }
        }
    }public boolean checkAccountChange(String username, String password) {
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getUsername().equals(username) && accountVi.get(i).getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
    public boolean checkRoom(String floor, String room) {
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getFloor().equals(floor) && accountVi.get(i).getRoomNum().equals(room)) {
                return true;
            }
        }
        return false;
    }
    public boolean checkUser(String username) {
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getUsername().equals(username)) {
                return false;
            }
        }
        return true;
    }@Override
    public int checkForChange(String username, String password) {
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getUsername().equals(username)) {
                if(accountVi.get(i).getPassword().equals(password)){
                    return 1;
                }else{
                    return 2;
                }
            }
        }
        currentAccount = null;
        return 3;
    }public void setNewPassword(String username,String password){
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getUsername().equals(username)) {
                accountVi.get(i).setPassword(password);
            }
        }
    }public String printRoom(String floor,String roomNumber){
        for (int i = 0; i < accountVi.size(); i++) {
            if (accountVi.get(i).getFloor().equals(floor) && accountVi.get(i).getRoomNum().equals(roomNumber)) {
                return  accountVi.get(i).toString();
            }
        }
        return "Name :" + "\nPhone number : "+
                "\nUsername : ";
    }

    public ArrayList<Visitor> toList() { return accountVi; }
}
