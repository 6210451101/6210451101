package controller.personcontroller;

public interface UserAdminDataSource {
    AccountAdmin getAccountData();
    void setAccountData(AccountAdmin account);
}
