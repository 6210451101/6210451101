package controller.personcontroller;

import person.AdminClass;

import java.util.ArrayList;

public class AccountAdmin implements DataAccount{
    private ArrayList<AdminClass> accountAd;
    private AdminClass currentAccount;
    public AccountAdmin() {accountAd= new ArrayList<>();}
    public void addAccount(AdminClass acc){
        accountAd.add(acc);
    }
    public AdminClass getCurrentAccount() {
        return currentAccount;
    }
    @Override
    public String toString() {
        return accountAd.toString();
    }
    public  boolean checkFile(){
        if(accountAd.size() == 0){
            return true;
        }
        return false;
    }public boolean checkAccountChange(String username, String password) {
        for (int i = 0; i < accountAd.size(); i++) {
            if (accountAd.get(i).getUsername().equals(username) && accountAd.get(i).getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
    @Override
    public boolean checkAccount(String username, String password) {
        for (int i = 0; i < accountAd.size(); i++) {
            if (accountAd.get(i).getUsername().equals(username) && accountAd.get(i).getPassword().equals(password)) {
                return true;
            }
        }
        currentAccount = null;
        return false;
    }

    @Override
    public int checkForChange(String username, String password) {
        for (int i = 0; i < accountAd.size(); i++) {
            if (accountAd.get(i).getUsername().equals(username)) {
                if(accountAd.get(i).getPassword().equals(password)){
                    return 1;
                }else{
                    return 2;
                }
            }
        }
        currentAccount = null;
        return 3;
    }

    public boolean checkUser(String username) {
        for (int i = 0; i < accountAd.size(); i++) {
            if (accountAd.get(i).getUsername().equals(username)) {
                return false;
            }
        }
        currentAccount = null;
        return true;
    }
    public void setNewPassword(String username,String password){
        for (int i = 0; i < accountAd.size(); i++) {
            if (accountAd.get(i).getUsername().equals(username)) {
                accountAd.get(i).setPassword(password);
            }
        }
    }
    public ArrayList<AdminClass> toList() { return accountAd; }
}
