package controller.personcontroller;

public interface UserDataSource {
    Account getAccountData();
    void setAccountData(Account account);
}
