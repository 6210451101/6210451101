package controller.personcontroller;

import person.User;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Account implements DataAccount {
    private ArrayList<User> account;
    private User currentAccount;

    public Account() {account = new ArrayList<>();}
    public void addAccount(User acc){ account.add(acc); }

    public User getCurrentAccount() {
        return currentAccount;
    }

    @Override
    public String toString() {
        return account.toString();
    }


    @Override
    public boolean checkAccount(String username, String password) {
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username) && account.get(i).getPassword().equals(password)) {
                currentAccount = account.get(i);
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                account.get(i).setTime(dtf.format(now));
                return true;
            }
        }
        currentAccount = null;
        return false;
    }public boolean checkAccountChange(String username, String password) {
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username) && account.get(i).getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }public void removeAccount(String username){
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username)) {
                account.remove(i);
            }
        }
    }
    public String name(String username){
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username)) {
                return account.get(i).getName();
            }
        }
        return "";
    }public String getImage(String username){
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username)) {
                return account.get(i).getImg();
            }
        }
        return "asdsw";
    }public void changeStatus(String user){
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(user) && account.get(i).getStatus().equals("Available")) {
                account.get(i).setStatus("Suspended");
            }else if (account.get(i).getUsername().equals(user) && account.get(i).getStatus().equals("Suspended")) {
                account.get(i).setStatus("Available");
            }
        }
    }
    public int getSize(){
        return account.size();
    }public String getAcc(int i){
        return account.get(i).getUsername();
    }

    public void setNewPassword(String username,String password){
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username)) {
                account.get(i).setPassword(password);
            }
        }
    }
    public boolean checkUser(String username) {
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username)) {
                return false;
            }
        }
        return true;
    }
    public int checkForChange(String username,String password) {
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username)) {
                if(account.get(i).getPassword().equals(password)){
                    return 1;
                }else{
                    return 2;
                }
            }
        }
        return 3;
    }
    public String checkName(String user){
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(user)) {
                return account.get(i).getName();
            }
        }
        return "";
    }public String getStatus(String username,String password){
        for (int i = 0; i < account.size(); i++) {
            if (account.get(i).getUsername().equals(username) &&
                    account.get(i).getPassword().equals(password)) {
                return account.get(i).getStatus();
            }
        }
        return "";
    }
    public ArrayList<User> toList() {
        return account;
    }

}
