package controller.personcontroller;

public interface UserVisitorDataSource {
    AccountVisitor getAccountData();
    void setAccountData(AccountVisitor account);
}
