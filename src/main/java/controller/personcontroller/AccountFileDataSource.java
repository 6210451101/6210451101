package controller.personcontroller;

import person.User;

import java.io.*;

public class AccountFileDataSource implements UserDataSource {
    private String fileDirectoryName;
    private String fileName;
    private Account account;

    public AccountFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }
    private void checkFileIsExisted(){
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            User acc = new User(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim(),data[5].trim());
            acc.setTime(data[4].trim());
            account.addAccount(acc);
        }
        reader.close();
    }

    @Override
    public Account getAccountData() {
        try {
            account = new Account();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return account;
    }

    @Override
    public void setAccountData(Account account) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {

            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (User acc: account.toList()) {

                String line = acc.getStatus() + ","
                        + acc.getName() + ","
                        + acc.getUsername() + ","
                        + acc.getPassword() + ","
                        + acc.getTime()+","+acc.getImg() ;
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
