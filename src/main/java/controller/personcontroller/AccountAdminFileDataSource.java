package controller.personcontroller;

import person.AdminClass;

import java.io.*;

public class AccountAdminFileDataSource implements UserAdminDataSource{
    private String fileDirectoryName;
    private String fileName;
    private AccountAdmin accountAd;

    public AccountAdminFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }
    private void checkFileIsExisted(){
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            AdminClass acc = new AdminClass(data[0].trim(), data[1].trim(),data[2].trim());
            accountAd.addAccount(acc);
        }
        reader.close();
    }
    @Override
    public AccountAdmin getAccountData() {
        try {
            accountAd = new AccountAdmin();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return accountAd;
    }

    @Override
    public void setAccountData(AccountAdmin accountAd) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {

            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (AdminClass acc: accountAd.toList()) {

                String line = acc.getName() + ","
                        + acc.getUsername() + ","
                        + acc.getPassword();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
