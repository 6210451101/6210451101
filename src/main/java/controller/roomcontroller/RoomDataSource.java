package controller.roomcontroller;

public interface RoomDataSource {
    RoomManagement getRoomData();
    void setRoomData(RoomManagement room);
}
