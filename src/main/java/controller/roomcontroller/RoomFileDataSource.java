package controller.roomcontroller;

import room.TypeRoom;

import java.io.*;

public class RoomFileDataSource implements RoomDataSource {
    private String fileDirectoryName;
    private String fileName;
    private RoomManagement room;

    public RoomFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }
    private void checkFileIsExisted(){
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            TypeRoom nrm = new TypeRoom(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim());
            room.addRoom(nrm);
        }
        reader.close();
    }
    @Override
    public RoomManagement getRoomData() {
        try {
            room = new RoomManagement();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return room;
    }

    @Override
    public void setRoomData(RoomManagement room) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {

            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (TypeRoom acc: room.toList()) {

                String line =  acc.getTypeRoom()+ "," + acc.getFloor() + ","
                        + acc.getRoomNumber() + ","
                        + acc.getStatus();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
