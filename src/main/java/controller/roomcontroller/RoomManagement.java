package controller.roomcontroller;


import room.TypeRoom;

import java.util.ArrayList;

public class RoomManagement {
    private ArrayList<TypeRoom> room;
    private TypeRoom currentRoom;
    public RoomManagement() {room = new ArrayList<>();}
    public void addRoom(TypeRoom nrm){
        room.add(nrm);
    }


    public TypeRoom getCurrentAccount() {
        return currentRoom;
    }

    @Override
    public String toString() {
        return room.toString();
    }

    public boolean checkRoom(String floor,String roomNumber) {
        for (int i = 0; i < room.size(); i++) {
            if (room.get(i).getFloor().equals(floor) && room.get(i).getRoomNumber().equals(roomNumber)) {
                return true;
            }
        }
        currentRoom = null;
        return false;
    }
    public String printRoom(String floor,String roomNumber){
        for (int i = 0; i < room.size(); i++) {
            if (room.get(i).getFloor().equals(floor) && room.get(i).getRoomNumber().equals(roomNumber)) {
                return  room.get(i).toString();
            }
        }
        return null;
    }public boolean checkStatus(String floor,String roomNumber) {
        for (int i = 0; i < room.size(); i++) {
            if (room.get(i).getFloor().equals(floor) && room.get(i).getRoomNumber().equals(roomNumber)) {
                if (room.get(i).getStatus().equals("No owner.")) {
                    return true;             //ห้องยังไม่มีใครจองสมัครได้
                }else{
                    return false;
                }
            }
        }
        return false;
    }public void changeStatus(String floor,String roomNumber) {
        for (int i = 0; i < room.size(); i++) {
            if (room.get(i).getFloor().equals(floor) && room.get(i).getRoomNumber().equals(roomNumber)) {
                room.get(i).setStatus("Already owned.");
            }
        }
        currentRoom = null;
    }public void changeStatusRemove(String roomNumber) {
        for (int i = 0; i < room.size(); i++) {
            if (room.get(i).getRoomNumber().equals(roomNumber)) {
                room.get(i).setStatus("No owner.");
            }
        }
        currentRoom = null;
    }
    public void roomVis(String roomNumber){
        for (int i = 0; i < room.size(); i++) {
            if (room.get(i).getRoomNumber().equals(roomNumber) || (roomNumber+"(").contains(room.get(i).getRoomNumber())) {
                room.get(i).setStatus("No owner.");
            }
        }
    }
    public boolean checkFile(){
        if (room.size() == 0){
            return true;
        }
        return false;
    }
    public ArrayList<TypeRoom> toList() { return room; }

}