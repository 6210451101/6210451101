package controller.importexportcontroller;

import importexport.ImportExportHistory;

import java.util.ArrayList;

public class ImportExportManager {
    private ArrayList<ImportExportHistory> histories;
    private ImportExportHistory currentHistory;
    public ImportExportManager() {histories = new ArrayList<>();}
    public void addAccount(ImportExportHistory his){ histories.add(his); }
    public void clearList(){
        histories.clear();
    }
    public ImportExportHistory getCurrentHistory() {
        return currentHistory;
    }

    @Override
    public String toString() {
        return histories.toString();
    }
    public ArrayList<ImportExportHistory> toList() {
        return histories;
    }

}
