package controller.importexportcontroller;

public interface ImportExportDataSource {
    ImportExportManager getAccountData();
    void setAccountData(ImportExportManager imEx);
}
