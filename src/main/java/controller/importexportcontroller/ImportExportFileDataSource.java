package controller.importexportcontroller;

import importexport.ImportExportHistory;

import java.io.*;

public class ImportExportFileDataSource implements ImportExportDataSource{
    private String fileDirectoryName;
    private String fileName;
    private ImportExportManager histories;

    public ImportExportFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }
    private void checkFileIsExisted(){
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            ImportExportHistory acc = new ImportExportHistory(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim(),data[4].trim());
            histories.addAccount(acc);
        }
        reader.close();
    }
    @Override
    public ImportExportManager getAccountData() {
        try {
            histories = new ImportExportManager();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return histories;
    }

    @Override
    public void setAccountData(ImportExportManager imEx) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {

            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (ImportExportHistory his: histories.toList()) {

                String line = his.getRoom() + ","
                        + his.getTypeMail() + ","
                        + his.getStatus() + ","
                        + his.getNameStaff() + ","
                        + his.getTime();
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
