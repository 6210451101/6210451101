package controller.importexportcontroller;

import mail.Letter;

import java.util.ArrayList;

public class MailManager {
    private ArrayList<Letter> mail;
    private Letter currentMail;
    public MailManager() {mail = new ArrayList<>();}
    public void addAccount(Letter m){
        mail.add(m);
    }


    public Letter getCurrentLetter() {
        return currentMail;
    }
    public String roomNum(int i){
        return mail.get(i).getRoomNum();
    }
    public void removeAccount(String room){
        for (int i = 0; i < mail.size(); i++) {
            if (mail.get(i).getRoomNum().equalsIgnoreCase(room)) {
                mail.remove(i);
                i--;
            }
        }
    }
    @Override
    public String toString() {
        return mail.toString();
    }

    public boolean checkRoom(String floor, String room) {
        for (int i = 0; i < mail.size(); i++) {
            if (mail.get(i).getFloor().equals(floor) && mail.get(i).getRoomNum().equals(room)) {
                currentMail = mail.get(i);
                return true;
            }
        }
        currentMail = null;
        return false;
    }public int sizeOfArray(){
        return mail.size();
    }
    public String printString(String room){
        for (int i = 0; i < mail.size(); i++) {
            if (mail.get(i).getRoomNum().equals(room)) {
                return mail.get(i).toString();
            }
        }
        return "";
    }public String getImage(String room){
        for (int i = 0; i < mail.size(); i++) {
            if (mail.get(i).getRoomNum().equals(room)) {
                return mail.get(i).getImage();
            }
        }
        return "";
    }public int sameNumRoom(String room){
        int k = 0;
        for (int i = 0; i < mail.size(); i++) {
            if (mail.get(i).getRoomNum().equals(room)) {
                k += 1;
            }
        }return k;
    }public Letter checkCurrent(int i , String room){
        if(mail.get(i).getRoomNum().equals(room)){
            currentMail = mail.get(i);
            return currentMail;
        }
        return null;
    }
    public ArrayList<Letter> toList() {
        return mail;
    }
}
