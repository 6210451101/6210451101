package importexport;

public class ImportExportHistory {
    private String room;
    private String typeMail;
    private String status;
    private String nameStaff;
    private String time;

    public ImportExportHistory(String room, String typeMail, String status, String nameStaff,String time) {
        this.room = room;
        this.typeMail = typeMail;
        this.status = status;
        this.nameStaff = nameStaff;
        this.time = time;
    }

    public String getRoom() { return room; }

    public String getTypeMail() { return typeMail; }

    public String getStatus() { return status; }

    public String getNameStaff() { return nameStaff; }

    public String getTime() {
        return time;
    }
}
