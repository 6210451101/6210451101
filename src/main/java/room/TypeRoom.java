package room;

public class TypeRoom {
    private String typeRoom;
    private String floor;
    private String roomNumber;
    private String status;

    public TypeRoom(String typeRoom, String floor, String roomNumber, String status) {
        this.typeRoom = typeRoom;
        this.floor = floor;
        this.roomNumber = roomNumber;
        this.status = status;
    }

    public String getFloor() { return floor; }
    public String getRoomNumber() { return roomNumber; }
    public String getStatus() { return status; }

    public String getTypeRoom() { return typeRoom; }

    public void setFloor(String floor) { this.floor = floor; }
    public void setRoomNumber(String roomNumber) { this.roomNumber = roomNumber; }
    public void setStatus(String status) { this.status = status; }

    @Override
    public String toString() {
        return "\nType : A bed room.\n" +
                "Floor : " + floor +
                " , Room Number : " + roomNumber  +
                "\nStatus : " + status ;

    }
}
