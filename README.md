Chaiyanon Klabkangwal 6210451101
Admin
Username: admin
Password: 1234

Jar File จะอยู่ใน SawaddeeTaweeSuk/PDF and jarFile
โครงสร้างไฟล์
package controller.personcontroller
มีไว้เก็บ Class ที่จำมีmethod ที่ไว้เช็คusername password
และในส่วนต่าง ของAccount admin ,เจ้าหน้าที่ส่วนกลาง ,ผู้พักอาศัย
และมีไว้เก็บ Class ที่มีไว้อ่านไฟล์ของ Account ทั้งหมด
package condo
มีไว้เก็บ Class ที่เป็นหน้าตาของแอพพลิเคชันทั้งหมด
package importexport
มีไว้เก็บ Class ที่เกี่ยวข้องกับการเก็บประวัติการนำเข้าและออกเอกสาร
package mail
มีไว้ใช้อ่านไฟล์ของ Document ,Letter and supplies และมี
Class ที่มีMethod เพื่อเช็คในการ Add หรือ Remove จดหมาย เอกสาร และพัสดุ
package person
มีไว้เก็บ Class ที่ไว้รับค่า เพื่อสมัคร Account ต่างๆ และนำส่งไปให้Class
ในpackage controller.personcontroller จัดการต่อ
package room
มีไว้สำหรับใช้อ่านไฟล์ ของประเภทห้องและชั้นและเลขที่ห้อง รวมไปถึงมี
method ในคลาสเพื่อนำมาใช้ประโยชน์ในการสมัครผู้เข้าพักอาศัย เช่น 
มีmethod checkStatus เพื่อ เช็คว่าห้องนี้มีใครพักอาศัยอยุ่หรือไม่ ถ้ามีก็ไม่สามารถ
สมัครได้
Commit ครั้งที่ 1 สร้างหน้าของApplication เกือบครบทุกหน้าและใส่หน้าที่ให้กับแต่ละButton
และสร้างClass สำหรับ Account ของผู้ดูแลระบบและส่วนกลาง ให้สามารถเช็คได้ว่าเป็นของใคร
Commit ครั้งที่ 2 สร้างInterface checkAccount เพื่อเช็คว่าเป็นusername และ password
ของส่วนกลางหรือAdmin 
Commit ครั้งที่ 3 สร้างคลาสแม่ ชื่อPerson และให้เกิดInheritance โดยมีClass User AdminClass
และVisitor เป็นClassลูก
Commit ครั้งที่ 4 ทำการสร้างไฟล์เพื่อจัดเก็บข้อมูลusername password name และ time เมื่อเข้า
ล่าสุด และนำการอ่านไฟล์มาใช้ และสร้างตารางTableView ในส่วนของAdmin เพื่อแสดงผลชื่อUsername และ
password และเวลาเข้าระบบล่าสุดของส่วนกลาง

Username และ Password ของAdmin คือ admin และ 1234 ตามลำดับ จะสามารถดูรายชื่อส่วนกลาง
และเวลาเข้าระบบล่าสุด และ สามารถสมัครAccount ของส่วนกลางได้ ในหน้าถัดไป และเมื่อสมัครข้อมูลจะถูก
เก็บในaccount.csv
เจ้าหน้าที่ส่วนกลาง สามารถ สมัคร Account ให้ผู้พักอาศัยได้ และสามารถ Add จดหมาย เอกสาร และพัสดุเข้าระบบได้
แต่ต้องมีเงื่อนไขว่า ห้องนั้นมี่ผู้พักอาศัยอยู่ มิฉนั้นจะไม่สามารถ Add ได้ เพราะมไ่มีคนอยู่นั่นเอง

**** การแก้ไข ****
ได้ทำการ เพิ่ม account ของเจ้าหน้าที่ส่วนกลางไว้ 2 คน 
คนที่ 1. Username : acc1
       Password : 1111
คนที่ 2. Username : acc2
       Password : 1111
ได้ทำการเพิ่ม Mail ไปยังระบบเพิ่อทดสอบเบื้องต้นแล้ว

ไดทำการ เพิ่ม account ของผู้เข้าพักอาศัยไว้ 2 คน
คนที่ 1. Username : vis1
       Password : 1111
คนที่ 2. Username : vis2
       Password : 1111
และในการ log in เข้าจะมีของที่มาส่งแสดงอยู่
(เป็นเพียงแค่การทดลองเพิ่ม ผู้ใช้Application สามารถเพิ่ม account เองได้)

1.ใช้คลาสเดียวในการใช้ร่วมกับของประเภทห้อง 2 ประเภท 1.1ห้องนอน 2.2ห้องนอน
โดยการเพิ่มตัวแปร typeRoomในClass Room เพื่อจะได้เก็บค่าเป็นประเภทห้อง

2.ก่อนหน้านี้เก็บประเภทห้องแยกกันเป็น 2 ไฟล์ csv แก้ไขเป็นการเก็บในไฟล์เดียวกัน
แต่Type ของห้องต่างกันได้ ตามหัวข้อที่ต้องแก็ไข "หลาย object สร้างจาก classเดียวได้ โดยระบุค่าเริ่มต้นต่างกันผ่าน constructor"
โดยรับค่าเริ่มต้นที่ต่างกันเลยจำแนกประเภทห้องได้

3. การโปรแกรมเชิงวัตถุขั้นสูง 
   3.1 แก้ไข Interface AllmailDataSource ที่มีmethod MailManager getAccountData();
   และ void setAccountData(MailManager letter,SetToFile s); เพื่อใช้ร่วมกับ AllMailFileDataSource โดยการ
   ใช้Controller เป็นclass MailManager
   3.2  สร้าง interface SetToFile ที่มี method ที่รับค่า object ของ class Letter ซึ่งเป็นclass แม่ของ Document และ Supplies
   โดย Class LetterDataForSetFile จะ implement SetToFile และใช้methodเพื่อ 
   return mail.getRecipName() + ","
                   + mail.getFloor() + ","
                   + mail.getRoomNum() + ","
                   + mail.getSender()+","
                   +mail.getWidth()+ ","
                   +mail.getLength() + ","
                   +mail.getImage();
   Class DocumentDataForSetFile จะ implement SetToFile และใช้method เพื่อ 
   return mail.getRecipName() + ","
                  + mail.getFloor() + ","
                  + mail.getRoomNum() + ","
                  + mail.getSender()+","
                  +mail.getImportant() + ","
                  +mail.getWidth()+ ","
                  +mail.getLength() + ","
                  +mail.getImage() ;
   และ Class SuppliesDataForSetFile จะ implement SetToFile และใช้metod เพื่อ
   return mail.getRecipName() + ","
                   + mail.getFloor() + ","
                   + mail.getRoomNum() + ","
                   + mail.getSender()+","
                   + mail.getCourier()+","
                   + mail.getTrackNum()+","
                   +mail.getWidth()+ ","
                   +mail.getLength() + ","
                   + mail.getHeight()+","
                   +mail.getImage();
   3.3 ในMailManager สามารถสร้างและทำงานเกี่ยวกับ Array list ของ Letter ซึ่งเป็นClass แม่ของ
   Document และ Supplies
   
4.ปรับแก้ไขปุ่มต่างๆ เช่น ปุ่มสำหรับข้ามไปหน้า Add account เจ้าหน้าที่ เปลี่ยนจาก Next เป็น Add account

5.แสดงรายการห้องพักทั้งหมดที่บอก ประเภท,ชั้น ,หมายเลขห้อง และ สถานะการพักอาศัย

6.หน้า Export ได้ทำการdisable table view เพื่อไม่ให้ขึ้น Highlight และการExport ก็ต้องเลือกห้อง
ว่าจะเอาจดหมาย, เอกสาร หรือ พัสดุของห้องใดออก ถ้าเลือก จดหมายของห้อง 101 จดหมายของห้อง101 จะถูกนำออกทั้งหมด
(การนำออก แยกกันสำหรับ จดหมาย, เอกสาร และ พัสดุ)

7.แก้ไขชื่อClass ต่างๆให้เหมาะสมยิ่งขึ้น

8.จัดเรียงmedel ออก จาก contrller

9.popup แจ้งเตือน remove room แก้ไขแล้ว

-----------*------------------
แก้ไข ลบ method ออกจาก Letter แล้วเวลาเรียกใช้method Document และ Supplies เปลี่ยนมาทำการ cast